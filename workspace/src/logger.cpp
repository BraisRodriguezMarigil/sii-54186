#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include "stdio.h"
#include "Puntuacion.h"

int main()
{
	int fd;
	Puntuacion puntos;

	if(mkfifo("/tmp/FIFO_LOGGER",0600) <0)
	{
		perror("No puede crearse el FIFO");
		return 1;
	}
	
	if ((fd=open("/tmp/FIFO_LOGGER", O_RDONLY))<0) 
	{
		perror("No puede abrirse el FIFO");
		return 1;
	}
	
	while(1)
	{
	if(read(fd, &puntos, sizeof(puntos))>=sizeof(puntos)) 
	{
		if(puntos.ultimo==2)
		{
			printf("Jugador 1 marca 1 punto, lleva un total de %d puntos.\n",puntos.jugador1);
		}
		if(puntos.ultimo==1)
		{
			printf("Jugador 2 marca 1 punto, lleva un total de %d puntos.\n",puntos.jugador2);
		}
		if(puntos.ultimo==0)
		{
			if(puntos.jugador1>puntos.jugador2)
			{
				printf("Jugador 1 marca 1 punto, lleva un total de %d puntos.\n",puntos.jugador1);
				printf("Partida finalizada.El ganador es el jugador 1\n");
			}
			if(puntos.jugador2>puntos.jugador1)
			{
				printf("Jugador 2 marca 1 punto, lleva un total de %d puntos.\n",puntos.jugador2);
				printf("Partida finalizada.El ganador es el jugador 2\n");		
			}
			close(fd);
			unlink("tmp/FIFO_LOGGER");
			_exit(0);
		}
	}
	}
	
}


