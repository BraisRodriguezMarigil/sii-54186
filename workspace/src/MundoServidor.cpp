//Autor: Brais Rodriguez Marigil
// MundoServidor.cpp: implementation of the CMundoServidor class.
//
//////////////////////////////////////////////////////////////////////

#include "MundoServidor.h"
#include"Puntuacion.h"
#include "glut.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include<iostream>
#include <sys/mman.h>
#include <fstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <pthread.h>
#include <sys/socket.h>




//////////////////////////////////////////////////////////////////////
//Variable global numero disparos
//////////////////////////////////////////////////////////////////////
#define NMAXDISPAROS 4
#define NMAXESFERAS 2
#define PERIODO 200
#define MAXGOLPES 2
int nDisparosJ1=0;
int nDisparosJ2=0;
int golpes1=0;
int golpes2=0;
bool bloqueo1=false;
bool bloqueo2=false;


//FUNCION PARA CERRAR EL SERVIDOR QUE SE EJECUTA CUANDO LLEGA LA SEÑAL SIGUSR"
static void TerminarProceso (int n)
{
	_exit(0);
}

static void TerminarProceso2 (int s)
{
	_exit(s);
}

void* hilo_comandos(void* d)
{
      CMundoServidor* p=(CMundoServidor*) d;
      p->RecibeComandosJugador();
}

void CMundoServidor::RecibeComandosJugador()
{

     while (1) {
            usleep(10);
            char cad[8];
            unsigned char key;
            
            comunicacion.Receive(cad,sizeof(cad));
            
            sscanf(cad,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
	    if(key=='d'){
		if(nDisparosJ1<NMAXDISPAROS){
		disparos.push_back(new Disparo(DERECHA,jugador1.getCentro()));
		nDisparosJ1+=1;
		}	
	    }
	    if(key=='k'){
		if(nDisparosJ2<NMAXDISPAROS){
		disparos.push_back(new Disparo(IZQUIERDA,jugador2.getCentro())); 
		nDisparosJ2+=1;
		}
		
	    }
      }
}

CMundoServidor::CMundoServidor()
{
	Init();
}

CMundoServidor::~CMundoServidor()
{
	
	if(unlink("/tmp/FIFO_LOGGER")<0){
		perror("Cerrar FIFO");
		exit(1);
	}
	
}

void CMundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();
	for(i=0;i<disparos.size();i++)
		disparos[i]->Dibuja();
	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoServidor::OnTimer(int value)
{	
	Puntuacion puntos;
	static int timer=0;
	
	if(!bloqueo1)
	jugador1.Mueve(0.025f);
	if(!bloqueo2)
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	
	for(int i=0;i<disparos.size();i++){
		disparos[i]->Mueve(0.025f);
	}
	for(int i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

//Gestion de los disparos
	for(int j=0;j<disparos.size();j++)
	{
		bool destruirDisparo=false;
			if(jugador1.recibeDisparo(*disparos[j])){
				
				jugador1.agrandar(-1);
				jugador1.velocidad.y=0;
				bloqueo1=true;
				destruirDisparo=true;
				nDisparosJ2-=1;
			}
			if(jugador2.recibeDisparo(*disparos[j])){
				jugador2.agrandar(-1);
				jugador2.velocidad.y=0;	
				bloqueo2=true;
				destruirDisparo=true;
				nDisparosJ1-=1;
			}
			if(fondo_izq.Rebota(*disparos[j])||fondo_dcho.Rebota(*disparos[j])){
				if(disparos[j]->d==DERECHA){nDisparosJ1-=1;}
				if(disparos[j]->d==IZQUIERDA){nDisparosJ2-=1;}
				destruirDisparo=true;		
			}
			if(destruirDisparo){
				delete disparos[j];
				disparos.erase(disparos.begin()+j);
				disparos.shrink_to_fit();
			}
			
			
			
		
	}
//Pasado un periodo de tiempo agranda la raqueta
	if(timer%PERIODO==0){
		jugador1.agrandar(1);
		jugador2.agrandar(1);
		timer=0;
		bloqueo1=false;
		bloqueo2=false;
	
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);

	if(fondo_izq.Rebota(esfera))
	{
		esfera.radio=0.5;
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		puntos.jugador1=puntos1;
		puntos.jugador2=puntos2;
		puntos.ultimo=1;
		write(fd,&puntos, sizeof(puntos));
	}

	if(fondo_dcho.Rebota(esfera))
	{	
		esfera.radio=0.5;
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		puntos.jugador1=puntos1;
		puntos.jugador2=puntos2;
		puntos.ultimo=2;
		write(fd,&puntos, sizeof(puntos));
	}

	if(puntos1==3 || puntos2==3)
	{
		puntos.ultimo=0;
		write(fd,&puntos, sizeof(puntos));
		kill(getpid(),SIGUSR2);
		
		
	}
	

	//CREAMOS LA CADENA QUE SE ENVIA DEL SERVIDOR AL CLIENTE CON LA INFO
	//DE LAS POSICIONES Y PUNTOS
	
	char cad[400];
	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %f %d %d",
		esfera.centro.x,esfera.centro.y, esfera.radio,
		jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,
		jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, 
		puntos1, puntos2);
//ENVIA LAS COORDENADAS A TRAVES DEL SOCKET AL CLIENTE
	comunicacion.Send(cad,sizeof(cad));

}

void CMundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{

}

void CMundoServidor::Init()
{
//Conexion del Socket
	char ip[]="127.0.0.1";
	if(conexion.InitServer(ip,8000)==-1)
		printf("error abriendo el servidor\n");
	comunicacion=conexion.Accept();
	//SE RECIBE EL NOMBRE DEL CLIENTE
	char nombre[50];
	comunicacion.Receive(nombre,sizeof(nombre));
	printf("El jugador %s se dispone a jugar .\n",nombre);
	
	
//INICIALIZACION DE LA FIFO CON LOGGER
	if ((fd=open("/tmp/FIFO_LOGGER", O_WRONLY))<0) {
		perror("No puede abrirse el FIFO_LOGGER");
		return;
	}
	


//ARMADO DE LA SEÑAL SIGPIPE

	struct sigaction accion;
	accion.sa_handler=&TerminarProceso;
	if (sigaction(SIGUSR2, &accion, NULL) < 0) {
		perror ("sigaction");
		return;
	}
	
	struct sigaction accion2;
	accion2.sa_handler=&TerminarProceso2;
	if (sigaction(SIGINT, &accion2, NULL) < 0) {
		perror ("sigaction");
		return;
	}
	if (sigaction(SIGTERM, &accion2, NULL) < 0) {
		perror ("sigaction");
		return;
	}
	if (sigaction(SIGPIPE, &accion2, NULL) < 0) {
		perror ("sigaction");
		return;
	}


//CREACION DEL THREAD

	pthread_attr_init(&atrib);
	pthread_attr_setdetachstate(&atrib, PTHREAD_CREATE_JOINABLE);
	pthread_create(&thid1, &atrib, hilo_comandos, this);


	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	
}


