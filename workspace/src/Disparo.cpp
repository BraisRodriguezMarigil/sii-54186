#include "Disparo.h"
#include"glut.h"
#define OFFSET	0.25

Disparo::Disparo(direccion d,Vector2D pos):d(d)
{
	if(d==DERECHA){
		velocidad.x=3;
		x1=pos.x+3*OFFSET;
		x2=pos.x+OFFSET;
		y1=pos.y+OFFSET;
		y2=pos.y-OFFSET;
	
	}
	else if (d==IZQUIERDA){
		velocidad.x=-3;
		x1=pos.x-OFFSET;
		x2=pos.x-3*OFFSET;
		y1=pos.y+OFFSET;
		y2=pos.y-OFFSET;
	}
	
}

Disparo::~Disparo()
{

}

void Disparo::Mueve(float t)
{

//Modificamos la posicion de las coordenadas x
	x1=x1+velocidad.x*t;
	x2=x2+velocidad.x*t;
//Modificamos la posicion de las coordenadas y
	y1=y1+velocidad.y*t;
	y2=y2+velocidad.y*t;	
}
void Disparo::Dibuja()
{
	glColor3f(r,g,b);
	glDisable(GL_LIGHTING);
	
	glBegin(GL_POLYGON);
		glVertex3d(x1,y1,1);
		glVertex3d(x1,y2,1);
		glVertex3d(x2,y2,1);	
		glVertex3d(x2,y1,1);	
	glEnd();
}
